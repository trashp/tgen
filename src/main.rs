use std::{
    collections::HashMap,
    env,
    fs::File,
    io::{stdin, stdout, Read, Write},
    path::PathBuf,
    thread::sleep,
    time::{Duration, Instant},
};

use fast_poisson::Poisson2D;
use flate2::{bufread::GzEncoder, Compression};
use rbf_interp::{PtValue, Rbf};

use svg::node::Text as TextV;
use svg::{
    node::element::{Circle, Line, Rectangle, Text},
    Document,
};
use valence_nbt::{compound, to_binary_writer, List};
use voronoice::{BoundingBox, Point, VoronoiBuilder};

type Pt = PtValue<f64>;

#[derive(Debug)]
struct Tile {
    x: f64,
    y: f64,
    voronoi_surface_area: f64,
    old_height: f64,
    height: f64,
    uplift: f64,
    neighbours: Vec<usize>,
    neighbours_dist: Vec<f64>,
    on_boundary: bool,

    flow_sources: Option<Vec<usize>>,
    flow_receiver: Option<usize>,
    tree_id: usize,
    drainage_area: f64,
}

const MAX_TALUS_ANGLE: f64 = 0.9; // radians
const THERMAL_EROSION_COF: f64 = 0.01;

fn pause() {
    let mut stdout = stdout();
    stdout.write(b"Press Enter to continue...\n").unwrap();
    sleep(Duration::from_secs(1));
    stdout.flush().unwrap();
    stdin().read(&mut [0]).unwrap();
}

fn main() {
    let dt = 2.5e5;
    let k = 5.61e-7;
    let max_uplift = 5e-4;

    // Init tiles
    let start = Instant::now();
    let mut tiles = init_tiles(max_uplift);
    let dur_init = start.elapsed();

    // Generate terrain iteratively
    let start = Instant::now();
    let mut i = 0;
    let n = 250;
    while i < n {
        let tree_amount = create_initial_stream_trees(&mut tiles);
        add_lake_drainage(&mut tiles, tree_amount);
        let tiles_bfs = compute_drainage_area(&mut tiles);
        erode(&mut tiles, tiles_bfs, dt, k);
        i += 1;
    }
    let dur_loop = start.elapsed();

    gen_svg(&mut tiles);
    generate_heightmap(&mut tiles);

    println!(
        "Time elapsed is: {:?}. Spent {:?} per loop.",
        (dur_init + dur_loop),
        (dur_loop / n)
    );

    // let file = File::open("clipboard.schem").unwrap();
    // let mut gzdecoder = GzDecoder::new(BufReader::new(file));
    // let mut buf= Vec::new();
    // gzdecoder.read_to_end(&mut buf).unwrap();

    // let (nbt, root_name) = from_binary_slice(&mut buf.as_slice()).unwrap();
    // println!("{:#?}", nbt)
}

fn gen_svg(tiles: &mut Vec<Tile>) {
    // Generate SVG from tiles.
    let mut doc = Document::new().set("viewBox", (0, 0, 400, 400));
    let rect = Rectangle::new().set("x", 0).set("y", 0);
    doc = doc.add(rect);
    for tile in tiles.iter() {
        // Add lines between tiles, will all be drawn twice because of lazy implementation.
        for neighbour_id in tile.neighbours.iter() {
            let neighbour = tiles.get(*neighbour_id).unwrap();
            let line = Line::new()
                .set("stroke", "gray")
                .set("stroke-width", 0.01)
                .set("x1", tile.x)
                .set("y1", tile.y)
                .set("x2", neighbour.x)
                .set("y2", neighbour.y);
            doc = doc.add(line);
        }

        // Add arrow from tile to receiver tile
        if let Some(rec_id) = tile.flow_receiver {
            let receiver = tiles.get(rec_id).unwrap();
            let mut line = Line::new()
                .set("x1", tile.x)
                .set("y1", tile.y)
                .set("x2", receiver.x)
                .set("y2", receiver.y)
                .set("stroke-width", 0.06);

            // If this is a line going between two different trees give it a different colour
            if receiver.tree_id != tile.tree_id {
                line = line.set("stroke", "rgb(255, 137, 46)");
            } else {
                line = line.set("stroke", "rgb(160, 42, 250)")
            }

            doc = doc.add(line);
        }
    }

    for tile in tiles.iter() {
        // Add circles representing the tiles
        let mut circle = Circle::new().set("cx", tile.x).set("cy", tile.y);
        if tile.on_boundary {
            circle = circle.set("fill", "rgb(2, 146, 230)").set("r", 0.15)
        } else {
            circle = circle.set("fill", "black").set("r", 0.1)
        }
        doc = doc.add(circle);

        // Display each tiles height
        let height = (tile.height * 100.).round() / 100.;
        let text_val = TextV::new(height.to_string());
        let text = Text::new()
            .add(text_val)
            .set("font-size", "0.05em")
            .set("x", tile.x + 0.05)
            .set("y", tile.y + 0.05);
        doc = doc.add(text)
    }

    svg::save("test.svg", &doc).unwrap();
}

#[inline(always)]
fn init_tiles(max_uplift: f64) -> Vec<Tile> {
    let img = image::open("R:/Projects/tgen/tgen.png")
        .unwrap()
        .grayscale()
        .to_rgb8();
    let width = img.width().into();
    let height = img.height().into();

    // Generate Poisson distribution
    let poisson_points: Vec<Point> = Poisson2D::new()
        .with_dimensions([width, height], 10.)
        .iter()
        .map(|[x, y]| Point { x, y })
        .collect();

    println!("Using {} samples", poisson_points.len());

    let mut tiles: Vec<Tile> = Vec::with_capacity(poisson_points.len());

    // Create Delaunay triangulation and Voronoi tesselation
    let voronoi = VoronoiBuilder::default()
        .set_sites(poisson_points)
        .set_bounding_box(BoundingBox::new(
            Point {
                x: width / 2.,
                y: height / 2.,
            },
            width - 0.5,
            height - 0.5,
        ))
        .set_lloyd_relaxation_iterations(1)
        .build()
        .unwrap();

    // Convert into Tiles
    voronoi.iter_cells().for_each(|cell| {
        let voronoi_surface_area = polygon_area(cell.iter_vertices().collect::<Vec<&Point>>());
        let x = cell.site_position().x;
        let y = cell.site_position().y;
        let u: f64 = img.get_pixel(x.round() as u32, y.round() as u32)[0].into();
        let uplift = ((u + 1.) / 256.) * max_uplift;

        tiles.push(Tile {
            x,
            y,
            voronoi_surface_area,
            old_height: 0.,
            height: 0.,
            uplift,
            neighbours: cell.iter_neighbors().collect(),
            neighbours_dist: Vec::new(),
            on_boundary: cell.is_on_hull(),

            flow_sources: None,
            flow_receiver: None,
            tree_id: 0,
            drainage_area: 0.,
        });
    });

    let mut i = 0;
    while i < tiles.len() {
        let (head, tail) = tiles.split_at_mut(i);
        let (tile, tail) = tail.split_first_mut().unwrap();
        for neighbour_id in tile.neighbours.iter() {
            let neighbour = get_around_hole(head, tail, i, *neighbour_id);

            tile.neighbours_dist.push(f64::sqrt(
                f64::powi(neighbour.x - tile.x, 2) + f64::powi(neighbour.y - tile.y, 2),
            ))
        }

        i += 1;
    }

    tiles
}

#[inline(always)]
fn polygon_area(points: Vec<&Point>) -> f64 {
    let mut area: f64 = 0.;

    let mut prev_point = *points.get(points.len() - 1).unwrap();
    for point in points {
        area += (point.x + prev_point.x) * (point.y * prev_point.y);

        prev_point = point;
    }

    area
}

#[inline(always)]
fn get_around_hole<'a, T>(
    head: &'a mut [T],
    tail: &'a mut [T],
    hole: usize,
    index: usize,
) -> &'a mut T {
    if index > hole {
        return tail.get_mut(index - (hole + 1)).unwrap();
    } else if hole > index {
        return head.get_mut(index).unwrap();
    } else {
        panic!("tried to get tile from hole")
    }
}

#[inline(always)]
fn create_initial_stream_trees(tiles: &mut Vec<Tile>) -> usize {
    tiles.iter_mut().for_each(|tile| {
        tile.flow_sources = None;
        tile.flow_receiver = None;
        tile.drainage_area = tile.voronoi_surface_area;
    });
    {
        let mut i = 0;
        while i < tiles.len() {
            // Split the tiles vec around the main tile so that its still possible to get other mutable references.
            let (head, tail) = tiles.split_at_mut(i);
            let (tile, tail) = tail.split_first_mut().unwrap();

            let mut min_neighbour_index = 0;
            let mut min_n_height = 0.;
            let mut first_loop = true;
            for neighbour_index in tile.neighbours.iter() {
                let neighbour = get_around_hole(head, tail, i, *neighbour_index);

                if first_loop {
                    min_neighbour_index = *neighbour_index;
                    min_n_height = neighbour.height;

                    first_loop = false;
                } else {
                    if min_n_height > neighbour.height {
                        min_neighbour_index = *neighbour_index;
                        min_n_height = neighbour.height;
                    }
                }
            }
            // If the lowest neighbouring tile is lower than this tile, set this tile as a flow source.
            if tile.height > min_n_height {
                let neighbour = get_around_hole(head, tail, i, min_neighbour_index);

                match neighbour.flow_sources {
                    Some(ref mut sources) => sources.push(i),
                    None => neighbour.flow_sources = Some(vec![i]),
                }

                tile.flow_receiver = Some(min_neighbour_index);
            }
            i += 1;
        }
    }

    fn rec_tree_id(tiles: &mut Vec<Tile>, tile_id: usize, tree_id: usize) {
        let flow_sources;
        {
            let tile = tiles.get_mut(tile_id).unwrap();
            tile.tree_id = tree_id;
            flow_sources = tile.flow_sources.clone();
        }

        if let Some(sources) = flow_sources {
            for source in sources {
                rec_tree_id(tiles, source, tree_id)
            }
        }
    }

    let mut i = 0;
    let mut tree_id = 0;
    while i < tiles.len() {
        let is_root;
        {
            let tile = tiles.get_mut(i).unwrap();
            is_root = tile.flow_receiver.is_none();
        }

        if is_root {
            rec_tree_id(tiles, i, tree_id);
            tree_id += 1;
        }

        i += 1;
    }

    tree_id
}

#[derive(Debug)]
struct LakeNode {
    lake_tile: Option<usize>,
    potential_connections: HashMap<usize, PotentialConnection>,
    connected: bool,
}

#[derive(Clone, Debug)]
struct PotentialConnection {
    from_lake: usize,
    to_tile_id: usize,
    pass_height: f64,
}

#[inline(always)]
fn add_lake_drainage(tiles: &mut Vec<Tile>, tree_amount: usize) {
    let mut lake_nodes = init_lake_nodes(tree_amount);
    let mut connected_nodes = add_potential_connections(tiles, &mut lake_nodes);

    // possibly super slow... :(
    // Go through lake nodes and make every lake connected by adding tile connections
    loop {
        // All the lakes on the border were set as connected at the start. now iterate over every connected node and look for the lowest incoming pass, and then add that lake to the list of connected lakes.
        // let lowest_connection = get_lowest_possible_connection(&mut lake_nodes);

        let mut lowest_connection: Option<PotentialConnection> = None;
        for lake_id in connected_nodes.iter() {
            if lake_nodes.len() <= *lake_id {
                println!("{:?}", connected_nodes);
                println!("{:#?}", lake_nodes);
            }

            let (head, tail) = lake_nodes.split_at_mut(*lake_id);
            let (lake, tail) = tail.split_first_mut().unwrap();

            for connection in lake.potential_connections.values() {
                let neighbour = get_around_hole(head, tail, *lake_id, connection.from_lake);

                if !neighbour.connected {
                    match lowest_connection {
                        Some(ref mut low_con) => {
                            if low_con.pass_height > connection.pass_height {
                                *low_con = connection.clone()
                            }
                        }
                        None => lowest_connection = Some(connection.clone()),
                    }
                }
            }
        }

        match lowest_connection {
            Some(connection) => {
                let lake = lake_nodes.get_mut(connection.from_lake).unwrap();
                let lake_tile = lake.lake_tile.expect("No lake tile set?");

                let lake_id;
                {
                    let tile = tiles.get_mut(connection.to_tile_id).unwrap();
                    lake_id = tile.tree_id;
                    match tile.flow_sources {
                        Some(ref mut sources) => sources.push(lake_tile),
                        None => tile.flow_sources = Some(vec![lake_tile]),
                    }
                }

                tiles.get_mut(lake_tile).unwrap().flow_receiver = Some(connection.to_tile_id);

                lake.connected = true;
                connected_nodes.push(lake_id);
            }
            None => break,
        }
    }
}

#[inline(always)]
fn init_lake_nodes(tree_amount: usize) -> Vec<LakeNode> {
    let mut lake_nodes = Vec::new();

    // Create list of lake_nodes
    let mut i = 0;
    while i < tree_amount {
        lake_nodes.push(LakeNode {
            lake_tile: None,
            potential_connections: HashMap::new(),
            connected: false,
        });

        i += 1;
    }

    lake_nodes
}

#[inline(always)]
fn add_potential_connections(tiles: &mut Vec<Tile>, lake_nodes: &mut Vec<LakeNode>) -> Vec<usize> {
    let mut connected_nodes = Vec::with_capacity(lake_nodes.len());

    // Iterate over every Tile and add potential LakeNode connections to create the lakes map
    let mut i = 0;
    while i < tiles.len() {
        let (head, tail) = tiles.split_at_mut(i);
        let (tile, tail) = tail.split_first_mut().unwrap();

        let lake = lake_nodes.get_mut(tile.tree_id).unwrap();

        // If this lake is on the outer boundary set it as connected so that we start here when connecting all the lakes later.
        if tile.on_boundary {
            lake.connected = true;
            // TODO: this probably occasionally puts the same lake in multiple times. that might cause some slowdowns
            connected_nodes.push(tile.tree_id);
        }

        // This is the lowest tile in the lake network
        if tile.flow_receiver.is_none() {
            lake.lake_tile = Some(i);
        }

        for neighbour_id in tile.neighbours.iter() {
            let neighbour = get_around_hole(head, tail, i, *neighbour_id);

            // Ignore neighbour if in the same tree.
            if neighbour.tree_id == tile.tree_id {
                continue;
            }

            let pass_height = f64::max(tile.height, neighbour.height);
            let pot_cons = &mut lake.potential_connections;

            match pot_cons.get_mut(&neighbour.tree_id) {
                Some(potential_con) => {
                    if pass_height < potential_con.pass_height {
                        potential_con.pass_height = pass_height;
                        potential_con.to_tile_id = i;
                    }
                }
                None => {
                    pot_cons.insert(
                        neighbour.tree_id,
                        PotentialConnection {
                            from_lake: neighbour.tree_id,
                            to_tile_id: i,
                            pass_height,
                        },
                    );
                }
            }
        }

        i += 1;
    }

    connected_nodes
}

#[inline(always)]
fn compute_drainage_area(tiles: &mut Vec<Tile>) -> Vec<usize> {
    let mut tile_bfs = Vec::with_capacity(tiles.len());
    let mut i = 0;
    while i < tiles.len() {
        let tile = tiles.get(i).unwrap();

        if tile.flow_receiver.is_none() {
            tile_bfs.push(i);
        }

        i += 1;
    }

    let mut i = 0;
    loop {
        let len = tile_bfs.len();
        while i < len {
            let tile = tiles.get(tile_bfs[i]).unwrap();
            if tile.flow_sources.is_some() {
                for id in tile.flow_sources.as_ref().unwrap() {
                    tile_bfs.push(*id);
                }
            }

            i += 1;
        }

        if len == tile_bfs.len() {
            break;
        }
    }

    tile_bfs.iter().rev().for_each(|id| {
        let (head, tail) = tiles.split_at_mut(*id);
        let (tile, tail) = tail.split_first_mut().unwrap();

        if let Some(receiver) = tile.flow_receiver {
            get_around_hole(head, tail, *id, receiver).drainage_area += tile.drainage_area;
        }
    });

    tile_bfs
}

#[inline(always)]
fn erode(tiles: &mut Vec<Tile>, tiles_bfs: Vec<usize>, dt: f64, k: f64) {
    for i in tiles_bfs {
        let (head, tail) = tiles.split_at_mut(i);
        let (tile, tail) = tail.split_first_mut().unwrap();

        let h_i = tile.height;
        let u = tile.uplift;
        let a = tile.drainage_area;
        let m = 0.55;

        if let Some(receiver_id) = tile.flow_receiver {
            let receiver = get_around_hole(head, tail, i, receiver_id);
            let h_j = receiver.height;

            let dist =
                f64::sqrt(f64::powi(tile.x + receiver.x, 2) + f64::powi(tile.y + receiver.y, 2));

            tile.old_height = tile.height;
            tile.height = (h_i + dt * (u + (k * f64::powf(a, m) * h_j) / (dist)))
                / (1. + (k * f64::powf(a, m) * dt) / dist);
        } else {
            // tile.height = h_i+ dt * u;
        }

        // let mut j = 0;
        // for neighbour_id in tile.neighbours.iter() {
        //     let neighbour = get_around_hole(head, tail, i, *neighbour_id);
        //     let dist = tile.neighbours_dist.get(j).unwrap();
        //     let height_dif = tile.old_height - neighbour.old_height;

        //     let angle = f64::sin(height_dif/dist);
        //     if angle > MAX_TALUS_ANGLE {
        //         tile.height -= height_dif * THERMAL_EROSION_COF;
        //         neighbour.height += height_dif * THERMAL_EROSION_COF;
        //     }
        //     j += 1;
        // }
        {
            let mut j = 0;
            while j < tile.neighbours.len() {
                let neighbour = get_around_hole(head, tail, i, *tile.neighbours.get(j).unwrap());
                let dist = tile.neighbours_dist.get(j).unwrap();
                let height_dif = tile.old_height - neighbour.old_height;

                let angle = f64::sin(height_dif / dist);
                if angle > MAX_TALUS_ANGLE && height_dif > 0. {
                    neighbour.height += height_dif * THERMAL_EROSION_COF;
                    tile.height = tile.height - height_dif * THERMAL_EROSION_COF;
                }

                j += 1;
            }
        }
    }
}

// This can probably be done better.
fn generate_heightmap(tiles: &mut Vec<Tile>) {
    let size = 565;
    env::set_var("RUST_BACKTRACE", "full");
    let mut points = Vec::with_capacity(tiles.len());

    for tile in tiles {
        points.push(Pt::new(tile.x, tile.y, tile.height));
    }

    let rbf = Rbf::new(&points, "thin_plate", None);

    let mut heighest_point = 0;
    let mut height_map = Vec::with_capacity(100);
    for x in 0..size {
        let mut row = Vec::with_capacity(100);
        for y in 0..size {
            let height = rbf.interp_point((x as f64, y as f64)).round() as u32;
            row.push(height);

            if height > heighest_point {
                heighest_point = height;
            }
        }
        height_map.push(row)
    }

    // let mut blocks = Vec::with_capacity(size * size * size);
    let mut blocks = Vec::new();
    for z in 0..heighest_point {
        for row in height_map.iter() {
            for surface_height in row {
                if z > *surface_height {
                    blocks.push(0_i8)
                } else {
                    blocks.push(1_i8)
                }
            }
        }
    }
    // Format from https://github.com/SpongePowered/Schematic-Specification/blob/master/versions/schematic-2.md
    let c = compound! {
            "Version" => 2_i32,
            "DataVersion" => 3105_i32,

            "Width" => size as i16,
            "Height" => heighest_point as i16,
            "Length" => size as i16,

            "PaletteMax" => 2_i32,
            "Palette" => compound!{
                "minecraft:air" => 0_i32,
                "minecraft:white_concrete" => 1_i32,
            },
            "BlockData" => blocks,

            "Entities" => List::Compound(vec![]),
    };

    let mut buf = Vec::new();
    to_binary_writer(&mut buf, &c, "Schematic").unwrap();
    println!("{}", buf.len());

    let mut buf2 = Vec::new();
    GzEncoder::new(buf.as_slice(), Compression::fast())
        .read_to_end(&mut buf2)
        .unwrap();
    println!("{}", buf2.len());

    let mut f = File::create(
        [
            "R:\\",
            "Projects",
            "Spigot Plugins",
            "Paper test server",
            "plugins",
            "FastAsyncWorldEdit",
            "schematics",
            "test.schem",
        ]
        .iter()
        .collect::<PathBuf>(),
    )
    .unwrap();
    f.write_all(&buf2).unwrap();
    println!("DONE");

    // R:\Projects\Spigot plugins\Paper test server\plugins\FastAsyncWorldEdit\schematics
}
